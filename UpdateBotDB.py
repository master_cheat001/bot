import os,json,shutil

PATH_ASSETS=os.path.join(os.path.realpath(__file__),*[os.pardir,os.pardir,'assets'])
dest = os.path.join(os.path.realpath(__file__),*[os.pardir,'res','configdata'])

def main():
	os.makedirs(dest, exist_ok=True)
	db={}
	p_cn = os.path.join(PATH_ASSETS,*['china','ExportAssetBundle','configdata','china'])
	p_gl = os.path.join(PATH_ASSETS,*['global','ExportAssetBundle','configdata','global'])
	for fp in os.listdir(p_cn):
		f_cn = os.path.join(p_cn,fp)
		f_gl = os.path.join(p_gl, fp)
		if not os.path.isfile(f_cn) or fp[:10]!='ConfigData' or fp[-9:]!='Info.json':
			continue
		try:
			data = {
				item['ID']:item
				for item in json.loads(open(f_cn,'rb').read())
			}
			if os.path.isfile(f_gl):
				Merge(data,{
					item['ID']:item
					for item in json.loads(open(f_gl,'rb').read())
				})
		except:
			print(fp)
			continue
		db[fp[10:-9]]=data
		open(os.path.join(dest,fp[10:-9]+'.json'),'wb').write(json.dumps(data,ensure_ascii=False, indent='\t').encode('utf8'))
	#open(os.path.join(dest,'database.json'),'wb').write(json.dumps(db,ensure_ascii=False, indent='\t').encode('utf8'))


def Merge(cn,gl):
	for key,item in gl.items():
		if key in cn:
			typ=type(item)

			if typ in [str,int]:
				cn[key]=item
			elif typ == list:
				pass
			elif typ == dict:
				cn[key] = Merge(cn[key],item)

	return cn

main()