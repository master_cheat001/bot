import os
import discord
import asyncio
import json
from discord.ext import commands
from utils import FindBest
import discord
import datetime
import utils.Langrisser as ToEmbed

PAGES=ToEmbed.settings.PAGES

class Langrisser_Cog(commands.Cog):
	def __init__(self, bot):
		self.bot = bot
		self.bot.remove_command('help')

	async def add_reactions(self,msg,reactions):
		for r in reactions:
			await msg.add_reaction(r)

	@commands.Cog.listener()
	async def on_ready(self):
		"""http://discordpy.readthedocs.io/en/rewrite/api.html#discord.on_ready"""
		print(f'\n\nLogged in as: {self.bot.user.name} - {self.bot.user.id}\nVersion: {discord.__version__}\n')
		# Changes our bots Playing Status. type=1(streaming) for a standard game you could remove type and url.
		#await bot.change_presence(game=discord.Game(name='Cogs Example', type=1, url='https://twitch.tv/kraken'))
		print(f'Successfully logged in and booted...!')
		await self.bot.change_presence(status=discord.Status.online, activity=discord.Game('.help'))

	#rng
	@commands.command(name='summonhero', aliases=['sh'],description='summons heroes')
	async def _summon_hero(self,ctx, *, name):
		try:
			name=int(name)
		except:
			pass
		msg = await ctx.send(embed=ToEmbed.Summon_Hero(name))

	@commands.command(name='summonarms', aliases=['sa'],description='summons heroes')
	async def _summon_arms(self,ctx, *, name):
		try:
			name=int(name)
		except:
			pass
		msg = await ctx.send(embed=ToEmbed.Summon_Arms(name))

	#datamine

	@commands.command(name='enchant', aliases=[],description='enchant value ranges')
	async def _enchant(self,ctx, *, name):
		item = FindBest([x for k,x in ToEmbed.PAGES['enchant'].items()], name,keys=False)
		embed=ToEmbed.Embed()
		embed.set_footer(text='enchant')
		msg = await ctx.send(ToEmbed.Enchant(item), embed=embed)
		await self.add_reactions(msg,PAGES['enchant'])


	@commands.command(name='map', aliases=['m','q','quest'],description='the map of a mission')
	async def _map(self,ctx, *, name):
		item = FindBest(ToEmbed.DIRS2['Battle'], name, ['Name'])
		image = ToEmbed.MapImage(item)
		embed=ToEmbed.Embed(title=ToEmbed.DIRS2['Battle'][item]['Name'])
		msg = await ctx.send(file=discord.File(image,filename='{}.png'.format(str(item))), embed=embed)


	rift_normal = {key:val for key,val in ToEmbed.DIRS2['RiftLevel'].items() if val['ChallengeCount']==10}
	rift_elite = {key:val for key,val in ToEmbed.DIRS2['RiftLevel'].items() if val['ChallengeCount']==3}
	@commands.command(name='rift', aliases=['r'],description='the map of a timerift mission')
	async def _rift(self,ctx, *, name):
		if 'e' in name:	#elite
			item = FindBest(self.rift_elite, name, ['Name','NameNum'])
		else:
			item = FindBest(self.rift_normal, name, ['Name','NameNum'])
		image = ToEmbed.MapImage(ToEmbed.DIRS2['RiftLevel'][item]['BattleID'])
		embed=ToEmbed.Embed(title=ToEmbed.DIRS2['RiftLevel'][item]['Name'])
		msg = await ctx.send(file=discord.File(image,filename='{}.png'.format(str(item))),embed=embed)

	@commands.command(name='chapter', aliases=['c'],description='the map of a chapter mission')
	async def _chapter(self,ctx, *, name):
		item = FindBest(ToEmbed.DIRS2['Scenario'], name, ['Name','Chapter','ID'])
		image = ToEmbed.MapImage(ToEmbed.DIRS2['Scenario'][item]['BattleID'])
		embed=ToEmbed.Embed(title=ToEmbed.DIRS2['Scenario'][item]['Name'])
		msg = await ctx.send(file=discord.File(image,filename='{}.png'.format(str(item))),embed=embed)

	@commands.command(name='jointbattle', aliases=['jb'],description='the map of a mission')
	async def _joint_battle(self,ctx):
		embed,image = ToEmbed.JointBattle()
		msg = await ctx.send(embed=embed,file=discord.File(image,filename='{}.png'.format(str(embed.title))))

	@commands.command(name='hero',aliases=['h','unit','u'],description='key data of the unit')
	async def hero(self,ctx, *, name):
		item = FindBest(ToEmbed.DIRS2['Hero'], name, ['NameEng'])
		msg = await ctx.send(embed=ToEmbed.Hero2(item))
		await self.add_reactions(msg,PAGES['hero'])

	@commands.command(name='soldier', aliases=['soldiers','s','sol'],description='key data of the unit')
	async def soldier(self,ctx, *, name):
		item = FindBest(ToEmbed.DIRS2['Soldier'], name, ['Name'])
		msg = await ctx.send(embed=ToEmbed.Soldier2(item))

	@commands.command(name='mats',aliases=[],description='materials required for the classes of the unit')
	async def mats(self,ctx, *, name):
		item = FindBest(ToEmbed.DIRS2['Hero'], name, ['NameEng'])
		msg = await ctx.send(embed=ToEmbed.Hero2(item,'mats'))
		await self.add_reactions(msg,PAGES['hero'])

	@commands.command(name='jobmat',aliases=['jm','jobmats'],description='data about the job material')
	async def jobmat(self,ctx, *, name):
		item = FindBest(ToEmbed.DIRS2['JobMaterial'], name, ['Name'])
		msg = await ctx.send(embed=ToEmbed.JobMats(item))

	@commands.command(name='skin',aliases=['skins','art'],description='skins of the unit')
	async def _skin(self,ctx, *, name):
		item = FindBest(ToEmbed.DIRS2['Hero'], name, ['NameEng'])
		for embed in ToEmbed.Hero2(item,'skins'):
			await ctx.send(embed=embed)

	@commands.command(name='faction', aliases=['f','allegiance'],description='key data of the unit')
	async def faction(self,ctx, *, name):
		item = FindBest(ToEmbed.DIRS2['HeroTag'], name, ['Name'])
		msg = await ctx.send(embed=ToEmbed.Faction(item))

	@commands.command(name='arms', aliases=['arm','gear','equip','equipment','a','e','g'],description='key data of the gear')
	async def arms(self,ctx, *, name):
		item = FindBest(ToEmbed.DIRS2['Equipment'], name, ['Name'])
		msg = await ctx.send(embed=ToEmbed.Arms2(item))


	#wiki
	# @commands.command(name='soldier', aliases=['soldiers','s','sol'],description='key data of the unit')
	# async def soldier(self,ctx, *, name):
	# 	item = FindBest(ToEmbed.DIRS['soldier'], name, ['name'])
	# 	msg = await ctx.send(embed=ToEmbed.Soldier(item))

	# @commands.command(name='hero', aliases=['h','unit','u'],description='key data of the unit')
	# async def hero(self,ctx, *, name):
	# 	item = FindBest(ToEmbed.DIRS['hero'], name, ['name'])
	# 	msg = await ctx.send(embed=ToEmbed.Hero(item))
	# 	await self.add_reactions(msg,PAGES['hero'])

	# @commands.command(name='arms', aliases=['arm','gear','equip','equipment','a','e','g'],description='key data of the unit')
	# async def arms(self,ctx, *, name):
	# 	item = FindBest(ToEmbed.DIRS['arms'], name, ['name'])
	# 	msg = await ctx.send(embed=ToEmbed.Arms(item))

	####	ETC ##############################################################################################
	# @commands.group(pass_context=True)
	# async def ETC(self,ctx):
	#	 if ctx.invoked_subcommand is None:
	#		 await self.bot.say('Invalid command passed...')

	@commands.command(name='emoji',description='converts reaction to unicode (for copypaste)')
	async def emoji(self,ctx):
		embed = discord.Embed(title="Emoji to Unicode", description='~emoji~', color=8355711)
		embed.set_footer(text='Emoji-Converter')
		await ctx.send(embed=embed)

	@commands.command(name='info',description='informations about the bot and holds the invite link')
	async def info(self,ctx):
			"""Bot Info"""
			embed = discord.Embed()
			embed.add_field(name="Author", value='<@281201917802315776>')
			embed.add_field(name="Library", value='discord.py (Python)')
			embed.add_field(name='Invite Link', value="[\\o/ (hidden link)](https://discordapp.com/oauth2/authorize?client_id=536886558096097292&scope=bot 'can only be invited by the server admin')")
			embed.add_field(name="Servers", value="{} servers".format(len(self.bot.guilds)))

			total_members = sum(len(s.members) for s in self.bot.guilds)
			total_online = sum(1 for m in self.bot.get_all_members() if m.status != discord.Status.offline)
			unique_members = set(map(lambda x: x.id, self.bot.get_all_members()))
			embed.add_field(name="Total Members", value='{} ({} online)'.format(total_members, total_online))
			embed.add_field(name="Unique Members", value='{}'.format(len(unique_members)))

			embed.set_footer(text='Made with discord.py', icon_url='http://i.imgur.com/5BFecvA.png')
			embed.set_thumbnail(url=self.bot.user.avatar_url)
			await ctx.send(embed=embed)	#delete_after=60, 

	@commands.command(name='help',description='description of all commands')
	async def help(self,ctx):
		embed=discord.Embed()
		for command in sorted(list(self.bot.commands), key=lambda c: c.name):
			name=command.name
			aliases=' ,'.join(command.aliases)
			description=command.description
			embed.add_field(name='%s%s'%(name,' [alias: %s]'%aliases if aliases else ''),value=description if description else '/',inline=False)
		await ctx.send(embed=embed)

	####	REACTIONS   ######################################################################################
	@commands.Cog.listener()
	async def on_raw_reaction_add(self,payload):
		#message_id #int – The message ID that got or lost a reaction.
		#user_id #int – The user ID who added or removed the reaction.
		#channel_id #int – The channel ID where the reaction got added or removed.
		#guild_id #Optional[int] – The guild ID where the reaction got added or removed, if applicable.
		#emoji #PartialEmoji – The custom or unicode emoji being used.
		try:
			msg = await self.bot.get_channel(payload.channel_id).fetch_message(payload.message_id)
		except Exception as e:
			print('Failed to handle {payload},{msg}\nJump-Url:{URL}'.format(
					payload=payload,
					msg=payload.message_id,
					URL='https://discordapp.com/channels/{server}/{channel}/{message}'.format(
						server=payload.guild_id,
						channel=payload.channel_id,
						message=payload.message_id
						)
					)
				)
			print(e)
			return 0

		if payload.user_id != self.bot.user.id and msg.author == self.bot.user:
			emoji = payload.emoji.name
			ftext=msg.embeds[0].footer.text
			etitle = msg.embeds[0].author.name

			if ftext == 'Emoji-Converter':
				embed2 = discord.Embed(title="Reaction", description='```'+emoji+'```', color=0x00FF00)
				embed2.set_footer(text='Emoji-Converter')
				await msg.edit(embed=embed2)

			# elif ftext == 'hero':
			# 	hero = FindBest(ToEmbed.DIRS['hero'], etitle)
			# 	await msg.edit(embed=ToEmbed.Hero(hero,PAGES['hero'][emoji]))

			elif ftext == 'hero2':
				hero = FindBest(ToEmbed.DIRS2['Hero'], etitle, ['NameEng'])
				await msg.edit(embed=ToEmbed.Hero2(hero,PAGES['hero'][emoji]))

			elif ftext == 'enchant':
				embed=ToEmbed.Embed()
				embed.set_footer(text='enchant')
				await msg.edit(content=ToEmbed.Enchant(PAGES['enchant'][emoji]), embed=embed)

# The setup fucntion below is neccesarry. Remember we give self.bot.add_cog() the name of the class in this case MembersCog.
# When we load the cog, we use the name of the file.
def setup(bot):
	bot.add_cog(Langrisser_Cog(bot))