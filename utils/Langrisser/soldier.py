from ._main import DIRS,Embed, DIRS2, SkillDescriptionAIO, convert_HTML,gitlink

def Soldier2(id):
	soldier=DIRS2['Soldier'][id]
	if soldier["IsEnemy"]:
		for id,sol in DIRS2['Soldier'].items():
			if sol['Name']==soldier['Name'] and sol["IsEnemy"]==False:
				soldier=sol

	#create basic embed
	embed= Embed(
		title=soldier['Name'], #page name
		)
	for n,sol in DIRS['soldier'].items():
		if sol['name']==soldier['Name']:
			embed.set_thumbnail(url=sol['img'])

	fields=[
		{'name':	'Typ',			'value':	'%s [%s]'%(DIRS2['Army'][soldier['ArmyID']]['Name'], 'Melee' if soldier['IsMelee'] else 'Ranged'), 'inline':True},
		{'name':	'Move',			'value':	soldier['MoveType'][9:],	'inline':	True},
		{'name':	'Description',	'value':	convert_HTML(soldier['Desc']),	'inline':	False},
		{'name':	'Skill',		'value':	SkillDescriptionAIO(soldier["SkillsID"]) if 'SkillsID' in soldier else '-/-',	'inline':False},
		{'name':	'Heroes',		'value':	', '.join([DIRS2['Hero'][i]['NameEng'] for i in soldier['GetSoldierHerosID']]) if 'GetSoldierHerosID' in soldier else '-/-'}
	]
	embed.ConvertFields(fields)
	return embed

def Soldier(iname):
	#get soldier dir
	soldier=DIRS['soldier'][iname]
	#create basic embed
	embed= Embed(
		title='%s (%s)'%(soldier['name'],soldier['type']), #page name
		url=soldier['url']  #page link
		)
	embed.set_thumbnail(url=soldier['img'])

	SingleStats=['Span','Move','S','W',]

	fields=([
		{'name':'Move'   ,'value': soldier['stats']['Move'] ,'inline':True},
		{'name':'Range'   ,'value':soldier['stats']['Span'] ,'inline':True},
	])
	if 'S' in soldier['stats']:
		fields.append({'name':'Strong Against'   ,'value':soldier['stats']['S'] ,'inline':True})
	if 'W' in soldier['stats']:  
		fields.append({'name':'Weak Against'   ,'value':soldier['stats']['W'] ,'inline':True})
	if 'expr' in soldier and len(soldier['expr'])>2:
		fields.append({'name':'Skill'   ,'value':soldier['expr'] ,'inline':True})
	fields+=[
		{'name':'Stats'   ,'value':', '.join(['%s %s'%(val,key) for key,val in soldier['stats'].items() if key not in SingleStats]) ,'inline':False},
		#{'name':''   ,'value':'' ,'inline':True}
	]
	embed.ConvertFields(fields)
	return embed