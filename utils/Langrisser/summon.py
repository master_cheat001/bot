from ._main import DIRS2, Embed
import random

Heroes={'SSR':[],'SR':[],'R':[],'N':[]}
for id,hero in DIRS2['Hero'].items():
	if not 'Useable' in hero or not hero['Useable']:
		continue
	Heroes[DIRS2['Rank'][hero['Rank']]['Desc']].append(hero['NameEng'])

Arms={'SSR':[],'SR':[],'R':[],'N':[]}
for id,arm in DIRS2['Equipment'].items():
	Arms[DIRS2['Rank'][arm['Rank']]['Desc']].append(arm['Name'])

def Summon_Hero(count):
	count=max(1,min(30,count))
	pulls={'SSR':[],'SR':[],'R':[]}
	for i in range(count):
		x=random.randint(1,100)
		if x<3:	#1,2 = SSR
			pulls['SSR'].append(random.choice(Heroes['SSR']))
		elif x<13:	# 10 ~ SR
			pulls['SR'].append(random.choice(Heroes['SR']))
		else:
			pulls['R'].append(random.choice(Heroes['R']))
	
	return Embed(
		description='\n\n'.join([
			'__%s__\n%s'%(rarity,'\n'.join(units))
			for rarity, units in pulls.items()
			if units
			])
	)

def Summon_Arms(count):
	count=max(1,min(30,count))
	pulls={'SSR':[],'SR':[],'R':[]}
	for i in range(count):
		x=random.randint(1,100)
		if x<3:	#1,2 = SSR
			pulls['SSR'].append(random.choice(Arms['SSR']))
		elif x<13:	# 10 ~ SR
			pulls['SR'].append(random.choice(Arms['SR']))
		else:
			pulls['R'].append(random.choice(Arms['R']))
	
	return Embed(
		description='\n\n'.join([
			'__%s__\n%s'%(rarity,'\n'.join(units))
			for rarity, units in pulls.items()
			if units
			])
	)