from ._main import Embed, DIRS2, gitlink, convert_HTML

def Faction(iname):
	#get hero dir
	faction=DIRS2['HeroTag'][iname]
	#create basic embed
	embed= Embed(
		title=faction['Name'], #page name
		#url=hero['url']  #page link
		)
	embed.set_thumbnail(url=gitlink(faction['Icon']))
	fields=[
		{'name':'Description'   ,'value':   convert_HTML(faction['Desc']) ,'inline':True},
		{'name':'Heroes'  ,'value':   '\n'.join(DIRS2['Hero'][uid]['NameEng'] for uid in faction['RelatedHerosID']), 'inline':True},
		{'name':'Buffer'  ,'value':   '\n'.join(DIRS2['Hero'][uid]['NameEng'] for uid in faction['SuperHeroID']), 'inline':True},
	]
	embed.ConvertFields(fields)
	return embed

