import discord
from discord.ext import commands
from os import environ
import asyncio
import sys, traceback


L_bot = commands.Bot(command_prefix='.', description='Langrisser Mobile bot, written by W0lf in discord.py')

# Here we load our extensions(cogs) listed above in [initial_extensions].
if __name__ == '__main__':
	L_bot.load_extension('cogs.Spy')
	L_bot.load_extension('cogs.Langrisser')

#Create the loop
loop = asyncio.get_event_loop()

#Create bot tasks ~ supports multiple bots
token=environ.get('DISCORD_BOT_TOKEN')
loop.create_task(L_bot.start(token, bot=True, reconnect=True, case_sensitive=False))

#Run loop and catch exception to stop it
try:
	loop.run_forever()
finally:
	loop.stop()
